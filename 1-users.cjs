const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


// Q1 Find all users who are interested in playing video games.


/* let interestVideoGames=Object.entries(users).filter((values) => {
       if(values[1].interests.includes("Playing Video Games")){
           return values[0];
       }
  });
  
//console.log(interestVideoGames);*/

// Q2 Find all users staying in Germany.
let stayInGermany = Object.entries(users).filter((values) => {
    if (values[1].nationality === "Germany") {
        return values[0];
    }
})
console.log(Object.fromEntries(stayInGermany));


// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
let result = Object.entries(users).sort((first, second) => {
    if (first[1].desgination.includes("Developer")) {
        return -1;
    }
    else if (first[1].desgination.includes("Intern")) {
        return 1;
    }
});
console.log(result);

// Q4 Find all users with masters Degree.
let mastersDegree = Object.entries(users).filter((values) => {
    if (values[1].qualification === "Masters") {
        return values[0];
    }
});
console.log(Object.fromEntries(mastersDegree));


// Q5 Group users based on their Programming language mentioned in their designation.


let groupUsers = Object.entries(users).reduce((accumulater, currentValue) => {
    if (accumulater[currentValue[1].desgination.includes]) {
        accumulater[currentValue[1].desgination] = currentValue[0];
    }
    else {
        accumulater[currentValue[1].desgination] = [];
        accumulater[currentValue[1].desgination] = currentValue[0];
    }

    return accumulater;
}, {})
console.log(groupUsers);